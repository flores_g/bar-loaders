let fs = require("fs");
let path = require("path");
let glob = require("glob");
let mkdirp = require("mkdirp");
let rimraf = require("rimraf");
let {promisify} = require("util");

require("marko/node-require");
require("marko/compiler").defaultOptions.writeToDisk = false;

// This script is used to generate html pages in `pages` directory.
// The templates are written using marko template language.
async function main() {
  if (process.argv.includes("--list")) list();
  if (process.argv.includes("--build")) build();
}

async function list() {
  // This script list available pages.
  let files = await promisify(glob)("pages/**/*.html.marko", {});
  let pages = files.map((page) => {
    page = page.replace(/^pages\//, "/");
    page = page.replace(/index.html.marko$/, "");
    return page;
  });
  pages = pages.sort();
  // eslint-disable-next-line no-console
  pages.forEach(page => console.log(page));
}

async function build() {
  // This script build pages that use marko template and output html files.
  await promisify(rimraf)("tmp/marko", {});
  let files = await promisify(glob)("pages/**/*.html.marko", {});
  files.forEach(async (markoFilePath) => {
    let markoFile = require(`${process.cwd()}/${markoFilePath}`);
    let writeStreamPath = `tmp/marko/${markoFilePath.replace(".marko", "")}`;
    await promisify(mkdirp)(path.dirname(writeStreamPath));
    let writeStream = fs.createWriteStream(writeStreamPath);
    markoFile.stream().pipe(writeStream);
  });
}

main();
