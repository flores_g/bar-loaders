let {promisify} = require("util");
let rimraf = require("rimraf");

// This script is executed after "npm run build" step.
// We primarily use it to handle uploading sourcemaps to sentry
// and then removing it.
async function main() {
  await deleteSourcemaps();
}

async function deleteSourcemaps() {
  await promisify(rimraf)("build/**/*.js.map", {});
  // eslint-disable-next-line no-console
  console.log("[postbuild] Deleted .js.map files");
}

main();
