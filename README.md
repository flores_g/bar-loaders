# Loading Bars

The website is live at https://wonderful-mirzakhani-24cf23.netlify.com/

This website uses React and Redux.

##Notes on the website

- In case https://pb-api.herokuapp.com/bars takes more than 5 seconds to answer (happens some times), then some default configuration will be loaded
- The behavior of the loading bar scaling in size after reaching the limit is a different mechanism from the standard bar-loading from 0% to 100%. The reason why the whole bar grows in size after reaching 100% is simply to give visual feedback that the content is still increasing. Please view that as a bonus and not in opposition of the rule "Can go over limit defined in API), but limit the bar itself and change its colour". Also note that this "oversized" effect is capped at 250%  
- Pushing to master deploys automatically online with Netlify at: https://wonderful-mirzakhani-24cf23.netlify.com/
- Pushing on Bitbucket also trigger the Pipeline which checks for the eslint and tests

##Room for improvement

I did my best to follow the best practices in most areas of the projects under limited time.
Here are the other areas that could have been improved given more time:

- CSS: sharing variables
- Testing: testing the reducers
- Testing: testing the react components
- Testing: End-to-End testing with TestCafe

## Requirements

- [Node.js](https://nodejs.org/en/download/package-manager/) (v8.0.0 or above)
- [npm](https://www.npmjs.com/get-npm) (v5.0.0 or above)

## Installation

`npm install`

You may need to run `npm install` again if new modules are added to the project.

## Development

- `npm run dev`: Run local web server (localhost:5000) and [webpack](https://webpack.js.org/).
- `npm run pages:list`: List available pages 

## Test

- `npm test`: Run [eslint](http://eslint.org/) and [jest](https://facebook.github.io/jest/)
- `npm run jest`: Run [jest](https://facebook.github.io/jest/)

## Build

- `npm run build`: Build assets for production environment in `build` directory
- `npm run webpack`: Run webpack in development mode
- `npm run webpack:production`: Run webpack in production mode
- `npm run webpack:analyze`: Run webpack assets analyzer