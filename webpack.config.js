let path = require("path");
let webpack = require("webpack");
let CleanWebpackPlugin = require("clean-webpack-plugin");
let CopyWebpackPlugin = require("copy-webpack-plugin");
let LiveReloadPlugin = require("webpack-livereload-plugin");
let GitRevisionPlugin = require("git-revision-webpack-plugin");

module.exports = {
  target: "web",
  entry: {
    "version.js": "./src/version.js",
    "index.js": "./src/views/index.js"
  },
  output: {
    path: path.join(__dirname, "build"),
    filename: "[name]"
  },
  resolve: {
    modules: [
      "node_modules",
      path.join(__dirname, "src")
    ]
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        query: {
          cacheDirectory: true
        },
        exclude: path.join(__dirname, "node_modules")
      },
      {
        test:   /\.css$/,
        loader: "style-loader!css-loader!postcss-loader"
      },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        loader: "file-loader?name=[hash:8]-[name].[ext]"
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.COMMITHASH": JSON.stringify(new GitRevisionPlugin().commithash())
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/), // Ignore all optional deps of moment.js
    new CleanWebpackPlugin(["build"]),
    new CopyWebpackPlugin([
      {from: "public"},
      {from: "tmp/marko/pages"}
    ]),
    new LiveReloadPlugin({appendScriptTag: true})
  ]
};
