import { constructBars, calcNextBarValue } from "./bar-utils";

describe("bar-utils.js", () => {
  describe("constructBars", () => {
    test("empty input gives empty output", () => {
      expect(constructBars([])).toHaveLength(0);
    });

    test("with default and max value", () => {
      let intialValues = [3, 10, 23];
      let limitValue = 92;

      let bars = constructBars(intialValues, limitValue);

      expect(bars).toHaveLength(intialValues.length);

      for (let i = 0; i < intialValues.length; ++i) {
        expect(bars[i].currentValue).toEqual(intialValues[i]);
        expect(bars[i].limitValue).toEqual(limitValue);
        expect(bars[i].index).toEqual(i);
      }
    });
  });

  describe("calcNextBarValue", () => {
    let bar = {
      currentValue: 50,
      maxValue: 100,
      index: 0
    };

    test("positive modifier, within range", () => {
      let modifierValue = 30;

      expect(calcNextBarValue(bar, modifierValue)).toEqual(bar.currentValue + modifierValue);
    });

    test("positive modifier, outside range", () => {
      let modifierValue = 1030;
      
      expect(calcNextBarValue(bar, modifierValue)).toEqual(bar.currentValue + modifierValue);
    });

    test("negative modifier, within range", () => {
      let modifierValue = -15;

      expect(calcNextBarValue(bar, modifierValue)).toEqual(bar.currentValue + modifierValue);
    });

    test("negative modifier, outside range, should be equal to 0", () => {
      let modifierValue = -1050;
      
      expect(calcNextBarValue(bar, modifierValue)).toEqual(0);
    });
  });
});