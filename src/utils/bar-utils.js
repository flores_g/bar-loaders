function constructBars(initialValues, limitValue) {
	return initialValues.map((initialValue, index) => {
		return {
			index: index,
			currentValue: initialValue,
			limitValue: limitValue
		};
	});
}

function calcNextBarValue(bar, modifierValue) {
	let potentialValue = bar.currentValue + modifierValue;

	return Math.max(potentialValue, 0);
}

export {
	constructBars,
	calcNextBarValue
};