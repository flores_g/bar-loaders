import "babel-polyfill";
import domready from "domready";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Provider, connect } from "react-redux";
import store from "../store";
import { setupButtons, setupBars, changeBarValue, selectBar} from "../actions";
import axios from "axios";
import Dashboard from "../components/Dashboard.jsx";

let apiUrl = "https://pb-api.herokuapp.com/bars";
axios.get(apiUrl, {
  timeout: 5000
}).then((response) => {
  store.dispatch(setupBars(response.data.bars, response.data.limit));
  store.dispatch(setupButtons(response.data.buttons.sort((a, b) => a - b)));
}).catch(() => {
  store.dispatch(setupBars([11, 22, 33, 44], 256));
  store.dispatch(setupButtons([-30, 10, 25, 40]));
});

const mapStateToProps = state => {
  return {
    bars: state.bars,
    selectedBarIndex: state.selectedBarIndex,
    buttons: state.buttons
  };
};

const mapActionsToProps = {
  setupButtons,
  setupBars,
  changeBarValue,
  selectBar
};

let Container = connect(mapStateToProps, mapActionsToProps)(Dashboard);

domready(() => {
  ReactDOM.render(
    <Provider store={store}>
      <Container/>
    </Provider>
    , document.querySelector("#root")
  );
});
