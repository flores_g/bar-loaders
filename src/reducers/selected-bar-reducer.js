export default function reducer(state = 0, action) {
  switch (action.type) {
    case "SELECT_BAR":
      return action.payload.barIndex;
    default:
      return state;
  }
}