import { constructBars, calcNextBarValue } from "../utils/bar-utils";

export default function reducer(state = [], action) {
  switch (action.type) {
    case "SETUP_BARS":
      return constructBars(action.payload.initialValues, action.payload.limitValue);

    case "CHANGE_BAR_VALUE": {
      let selectedBar = state[action.payload.selectedBarIndex];
      let newValue = calcNextBarValue(selectedBar, action.payload.modifierValue);

      return state.map(bar => (
        bar.index == selectedBar.index ?
          {...bar, currentValue: newValue}
        :
          bar
      ));
    } default:
      return state;
  }
}