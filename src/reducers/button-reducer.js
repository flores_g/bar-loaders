export default function reducer(state = [], action) {
  switch (action.type) {
    case "SETUP_BUTTONS":
      return action.payload.buttonValues;
    default:
      return state;
  }
}