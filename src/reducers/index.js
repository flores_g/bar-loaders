import { combineReducers } from "redux";
import barReducer from "./bar-reducer";
import selectedBarReducer from "./selected-bar-reducer";
import buttonReducer from "./button-reducer";

export default combineReducers({
  bars: barReducer,
  selectedBarIndex: selectedBarReducer,
  buttons: buttonReducer
});