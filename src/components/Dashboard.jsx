import React from "react";
import autoBind from "react-autobind";
import LoadingBar from "./LoadingBar.jsx";
import "./Dashboard.css";

export default class Dashboard extends React.PureComponent {
  constructor(...args) {
    super(...args);
    autoBind(this);
  }

  changeBarValue(modifierValue) {
    this.props.changeBarValue(this.props.selectedBarIndex, modifierValue);
  }

  render() {
    return (
      <div id="dashboard">
        <section className="top">
          <h1> Loadin-Barz </h1>

          { this.props.bars.map(bar => (
            <LoadingBar key={ bar.index } bar={ bar } selectBar={ this.props.selectBar }
              isSelected={ bar.index == this.props.selectedBarIndex }/>
          ))}
        </section>
        <section className="bottom">
          <div className="buttons-list">
            { this.props.buttons.map( (buttonValue, index) => (
              <button key={ index } onClick={this.changeBarValue.bind(this, buttonValue)}
                className={`${buttonValue < 0 ? "negative" : "positive"}`}>
                { buttonValue }
              </button>
            ))}
          </div>
      </section>
      </div>
    );
  }
}
