import React from "react";
import autoBind from "react-autobind";
import classNames from "classnames";
import "./LoadingBar.css";

export default class LoadingBar extends React.PureComponent {
  constructor(...args) {
    super(...args);
    autoBind(this);
  }

  getRealPercentage() {
    return (this.props.bar.currentValue / this.props.bar.limitValue) * 100;
  }

  getEffectivePercentage() {
    return Math.min(this.getRealPercentage(), 100);
  }

  getOverflowCustomStyle() {
    if (!this.isOverflow()) {
      return {};
    } else {
      let perceivedRatio = Math.min(this.getRealPercentage() / 100, 2.5);
      let scaleModifier = 0.4;

      let xScale = 1 + (perceivedRatio - 1) * scaleModifier / 2;
      let yScale = 1 + (perceivedRatio - 1) * scaleModifier;

      let res = {
        transform: `scaleX(${xScale}) scaleY(${yScale})`
      };
      return res;
    }
  }

  isOverflow() {
    return this.props.bar.currentValue > this.props.bar.limitValue;
  }

  onSelectBar() {
    this.props.selectBar(this.props.bar.index);
  }

  render() {
    return (
      <div className={classNames("loading-bar", "clickable-block", {"is-selected": this.props.isSelected})}
        style={this.getOverflowCustomStyle()} onClick={this.onSelectBar.bind(this)}>

        <div className={classNames("progress-meter", {"is-overflow": this.isOverflow()})}
          style={{width: `${this.getEffectivePercentage()}%`}}>
        </div>
        <div className="value">{ this.props.bar.currentValue }</div>

      </div>
    );
  }
}