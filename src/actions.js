export function setupButtons(buttonValues) {
    return {
      type: "SETUP_BUTTONS",
      payload: {
        buttonValues: buttonValues
    }
  };
}

export function setupBars(initialValues, limitValue) {
  return {
    type: "SETUP_BARS",
    payload: {
      initialValues: initialValues,
      limitValue: limitValue
    }
  };
}

export function changeBarValue(selectedBarIndex, modifierValue) {
  return {
    type: "CHANGE_BAR_VALUE",
    payload: {
      selectedBarIndex: selectedBarIndex,
      modifierValue: modifierValue
    }
  };
}

export function selectBar(barIndex) {
  return {
    type: "SELECT_BAR",
    payload: {
      barIndex: barIndex
    }
  };
}
