let path = require("path");

module.exports = {
  plugins: {
    "postcss-import": {
      path: path.join(__dirname, "src")
    },
    "postcss-mixins": {},
    "postcss-custom-properties": {
      preserve: false
    },
    "postcss-calc": {},
    "postcss-color-function": {},
    "postcss-color-gray": {},
    "postcss-color-hsl": {},
    "postcss-font-family-system-ui": {},
    "postcss-nested": {},
    "autoprefixer": {}
  }
};
